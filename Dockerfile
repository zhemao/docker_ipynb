FROM base/arch
MAINTAINER Howard Mao, zhehao.mao@gmail.com

# Backup servers in case mirrors.kernel.org has trouble
RUN echo "Server = http://mirror.cc.columbia.edu/pub/linux/archlinux/$repo/os/$arch" >> /etc/pacman.d/mirrorlist
RUN echo "Server = http://mirrors.rutgers.edu/archlinux/$repo/os/$arch" >> /etc/pacman.d/mirrorlist

# Make sure we are up to date; Always use --noconfirm for pacman
RUN pacman -Syu --noconfirm
# Install IPython
RUN pacman -S ipython2 python2-pyzmq python2-tornado python2-jinja --noconfirm

# Install Supervisor
RUN pacman -S supervisor --noconfirm
ADD conf/supervisor/ipynotebook.ini /etc/supervisor.d/ipynotebook.ini
EXPOSE 8888

RUN useradd -m -d /home/runner -s /bin/bash runner
RUN echo runner:dockeripython | chpasswd

# Setup notebook profiles
RUN pacman -S sudo --noconfirm
ENV USER runner
ENV HOME /home/runner
RUN sudo -u runner ipython2 profile create docker
RUN sudo -u runner mkdir /home/runner/notebooks
ADD conf/ipynotebook/ipython_notebook_config.py \
        /home/runner/.config/ipython/profile_docker/ipython_notebook_config.py

CMD /usr/bin/supervisord -n 
